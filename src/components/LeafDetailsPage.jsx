import React from "react";
import { useParams } from "react-router-dom";
import "./LeafDetailsPage.css";

function LeafDetailsPage() {
  const { name } = useParams();
  return (
    <div className="videoContainer">
      <div className="leafCard">
        <img src={`/${name}.jpg`} className="leafd" />
        <button className="buy">Buy Pesticides</button>
      </div>
      <div className="videoCard">
        <h1>
          Watch video on how <br />
          to use the Pesticide
        </h1>
        <button
          type="button"
          className="btn btn-primary"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
        >
          Click to Watch
        </button>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <div className="ratio ratio-21x9">
                  <iframe
                    src="https://www.youtube.com/embed/kT0Lrpywf38"
                    title="YouTube video"
                    allowFullScreen=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LeafDetailsPage;
