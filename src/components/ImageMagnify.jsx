import React from "react";
import ReactImageMagnify from "react-image-magnify";

function ImageMagnify({ name, imageUrl }) {
  return (
    <ReactImageMagnify
      {...{
        smallImage: {
          alt: name,
          isFluidWidth: true,
          src: imageUrl,
        },
        largeImage: {
          src: imageUrl,
          width: 1200,
          height: 1800,
        },
        enlargedImagePosition: "over",
        enlargedImageContainerDimensions: {
          width: "100%",
          height: "100%",
        },
      }}
      className="leaf"
    />
  );
}

export default ImageMagnify;
