import React, { useState } from "react";
import { Link } from "react-router-dom";
import ReactImageMagnify from "react-image-magnify";
import "./LeafUi.css";

const angles = [0, 60, 120, 180, 240, 300];

function LeafUI() {
  const [zoomData, setZoomData] = useState("/leaf");
  const mangoImageUrl = "./mango2.jpg";

  const imageWidth = 500;
  const imageHeight = 500;
  const centerX = imageWidth / 2;
  const centerY = imageHeight / 2;
  const radius = Math.min(imageWidth, imageHeight) / 4;

  const calculateCircleCoordinates = (centerX, centerY, radius, angle) => {
    const radians = (angle * Math.PI) / 180;
    const x = centerX + radius * Math.cos(radians);
    const y = centerY + radius * Math.sin(radians);
    return `${x},${y},${radius}`;
  };

  const handleImageClick = () => {
    // console.log("initial", zoomData);
    setZoomData("/leaf/leafd1");
  };
  const handleWrongImageClick = () => {
    alert("Leaf growing well");
  };

  const handleAreaClick = (index) => {
    // console.log("Area", zoomData, index, event.target.coords);
    setZoomData(`/leaf/leafd${index + 1}`);
  };

  return (
    <div className=" container">
      <div className="card">
        <Link to={zoomData}>
          <ReactImageMagnify
            {...{
              smallImage: {
                alt: "mango plant",
                isFluidWidth: true,
                src: mangoImageUrl,
                onLoad: handleImageClick,
                onError: handleWrongImageClick,
              },
              largeImage: {
                src: mangoImageUrl,
                width: 1200,
                height: 1800,
              },
              enlargedImagePosition: "over",
              enlargedImageContainerDimensions: {
                width: "100%",
                height: "100%",
              },
            }}
            className='image'
          />
        </Link>
        <h1>Mango Plant</h1>
      </div>
      <div className=" card">
        <img
          useMap="#imageLeaf"
          src="/mango1.png"
          alt="MDN infographic"
          className="image"
        />
        <map name="imageLeaf">
          <Link to={zoomData}>
            {angles.map((angle, index) => (
              <area
                key={angle}
                shape="circle"
                coords={calculateCircleCoordinates(
                  centerX,
                  centerY,
                  radius,
                  angle
                )}
                alt={`Leaf ${angle / 60 + 1}`}
                onMouseOver={() => handleAreaClick(index)}
              />
            ))}
          </Link>
        </map>
        <h1>Mango Plant</h1>
      </div>
      <div className="card">
        <Link to="/leaf">
          <img src="./2.jpg" alt="image" className="image" />
        </Link>
        <h1>Promogranate Plant</h1>
      </div>
      <div className="card">
        <Link to="/leaf">
          <img src="./4.jpg" alt="image" className="image" />
        </Link>
        <h1>Promogranate Plant</h1>
      </div>
    </div>
  );
}

export default LeafUI;
