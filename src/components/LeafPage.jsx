import React from "react";
import { Link } from "react-router-dom";
import InnerImageZoom from "react-inner-image-zoom";
import ImageMagnify from "./ImageMagnify";
import "react-inner-image-zoom/lib/InnerImageZoom/styles.css";
import "./LeafPage.css";

function LeafPage() {
  const imageUrl = "./leaf2.jpg";

  return (
    <div className="leafContainer">
      <h1>List of Possible Diseases</h1>
      <div className="imageContainer">
        <div className="imageCard">
          <h1>Leaf 1</h1>
          <Link to="/leaf/leafd1">
            <ImageMagnify name="leaf" imageUrl="./leaf1.jpg" />
          </Link>
        </div>
        <div className="imageCard">
          <h1>Leaf 2</h1>
          <Link to="/leaf/leafd2">
            <ImageMagnify name="leaf" imageUrl="./leaf2.jpg" />
          </Link>
        </div>
        <div className="imageCard">
          <h1>Leaf 3</h1>
          <Link to="/leaf/3">
            <InnerImageZoom
              src="./leaf3.jpg"
              zoomSrc="./leaf3.jpg"
              className="leaf"
              zoomType="hover"
            />
          </Link>
        </div>
        <div className="imageCard">
          <h1>Leaf 4</h1>
          <Link to="/leaf/leafd1">
            <InnerImageZoom
              src="./leaf4.jpg"
              zoomSrc="./leaf4.jpg"
              className="leaf"
              zoomType="hover"
            />
          </Link>
        </div>
        <div className="imageCard">
          <h1>Leaf 4</h1>
          <Link to="/leaf/fruit1">
            <InnerImageZoom
              src="./fruit3.jpeg"
              zoomSrc="./fruit3.jpeg"
              className="leaf"
              zoomType="hover"
            />
          </Link>
        </div>
        <div className="imageCard">
          <h1>Leaf 5</h1>
          <Link to="/leaf/fruit1">
            <ImageMagnify name="fruit" imageUrl="./fruit2.jpg" />
          </Link>
        </div>
      </div>
    </div>
  );
}

export default LeafPage;
