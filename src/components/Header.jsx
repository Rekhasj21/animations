import React from "react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <nav className="navbar bg-body-tertiary bg-success">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          Home
        </Link>
      </div>
    </nav>
  );
}

export default Header;
