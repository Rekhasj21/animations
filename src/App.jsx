import React from "react";
import LeafUI from "./components/LeafUI";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LeafPage from "./components/LeafPage";
import LeafDetailsPage from "./components/LeafDetailsPage";
import Header from "./components/Header";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<LeafUI />} />
        <Route path="/leaf" element={<LeafPage />} />
        <Route path="/leaf/:name" element={<LeafDetailsPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
